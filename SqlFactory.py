from include.utilitiesModule.BigQueryOperations import BigQueryOperations
from include.utilitiesModule.ConnTypes import ConnType
from include.utilitiesModule.MsSqlOperations import MsSqlOperations
from include.utilitiesModule.MySqlOperations import MySqlOperations


class SqlFactory:
    """
    SqlFactory
    [Sql factory for initialization of SQL Operation abstract object (The implementation depends on the connection type)]
    """

    def __init__(self) -> None:
        """
        __init_
        """
        print("SqlFactoryInit")

    def create_sql(self, conn_type: ConnType):
        """
        create_sql [summary]

        Args:
            conn_type (str): Connection type

        Returns:
            SqlOperation: Returns the concrete implementation of sql operation abstract class, related to the connection type
        """

        print(f"SqlFactory.Create_sql: {conn_type} ")
        # print("type(conn_type)")
        # print(type(conn_type))
        # print("conn_type")
        # print(conn_type)

        if str(conn_type) == str(ConnType.google_cloud_platform):
            print("Big query")
            return BigQueryOperations()
        elif str(conn_type) == str(ConnType.mssql):
            print("Ms SQL")
            return MsSqlOperations()
        elif str(conn_type) == str(ConnType.mysql):
            print("My SQL")
            return MySqlOperations()

        raise NotImplementedError("Sql operation class not implemented")


if __name__ == "__main__":
    factory = SqlFactory()
    sql = factory.create_sql(conn_type=ConnType.google_cloud_platform)
    print(
        sql.get_connection_string(
            conn_object={
                "extra__google_cloud_platform__keyfile_dict": {"project": "airflow"}
            }
        )
    )
