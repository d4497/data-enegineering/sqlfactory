# DB Connection
import pyodbc
import pandas as pd
from include.utilities import get_connection_engine
from sqlalchemy import engine
from airflow.decorators import task
from airflow.hooks.base import BaseHook


@task(do_xcom_push=False)
class SPExecutor:
    """Executes SPs and logs their outputs"""

    def __init__(self, query: str, con: str):
        """Initializes the class and executes two private methods: the connection and the SP execution."""
        self.con = con
        self.query = query
        self._connect_()
        self._execute_(query)

    def _connect_(self):
        """Makes the connection with the BD using PyODBC driver"""
        connection = BaseHook.get_connection(self.con)
        print(f"AIRFLOW_CONN_{connection.conn_id.upper()}='{connection.get_uri()}'")
        host = connection.host
        db = connection.schema
        login = connection.login
        password = connection.password
        driver = "{ODBC Driver 17 for SQL Server}"

        connection_string = (
            "Driver="
            + driver
            + ";Server="
            + host
            + ";Database="
            + db
            + ";UID="
            + login
            + ";PWD="
            + password
            + ";)"
        )
        self.connect = pyodbc.connect(connection_string)

    def _execute_(self, query):
        """Runs the SP and logs"""
        with (conn := self.connect):
            cursor = conn.cursor()
            print(self.query)
            cursor.execute(self.query)

            self._print_sql_logs_(cursor)
            cursor.commit()

    def _print_sql_logs_(self, cursor: pyodbc.Cursor) -> None:
        """Prints SQL messeges if there is any."""
        print(self._pretify_log_(cursor.messages))
        while cursor.nextset():
            if not cursor.messages:
                continue
            print(self._pretify_log_(cursor.messages))

    def _pretify_log_(self, message: tuple) -> str:
        """Removes the driver info from a SQL message"""
        _, msg = message[0]
        return msg.replace("[Microsoft][ODBC Driver 17 for SQL Server][SQL Server]", "")


def table_exists(cursor: engine, schema: str, table_name: str):
    """checks whether a table exists in a database or not

    Args:
        cursor (sqlalchemy.engine): database conection
        schema (str): table schema name
        table_name (str): table name

    Returns:
        [bool]: existence of the table
    """
    cursor.execute(
        f"""IF EXISTS
            (
            SELECT 
                    *
                FROM INFORMATION_SCHEMA.TABLES WITH(NOLOCK)
                WHERE TABLE_NAME = '{table_name}' 
                AND TABLE_SCHEMA = '{schema}'
            )
            BEGIN
                SELECT 
                    'Your table exists.' AS result;
            END;
        """
    )

    try:
        cursor.fetchall()
        return True
    except:
        return False


def convertir_tipo(col: str, dtype: str):
    """maps pandas dtype to sql dtypes for turbodbc

    Args:
        col (str): column to be map
        dtype (str): pandas dtype for that column

    Returns:
        [string]: tipo de dato mapeado para la columna
    """
    conversion_tipos = {
        "int64": "BIGINT",
        "object": "VARCHAR(MAX)",
        "float64": "DECIMAL(38,10)",
        "bool": "TINYINT",
        # "datetime64": "DATETIME2(7)",
        # "datetime64[ns]": "DATETIME2(7)",
        "datetime64": "VARCHAR(MAX)",
        "datetime64[ns]": "VARCHAR(MAX)",
        "category": "VARCHAR(MAX)",
    }
    print(
        f"Columna: {col}, "
        f"dtype: {str(dtype)}, "
        f"Transformación de tipo: {conversion_tipos[str(dtype)]}"
    )
    if dtype in conversion_tipos.keys():
        return conversion_tipos[dtype]

    return "VARCHAR(MAX)"


def create_table(columnas: list, df: pd.DataFrame, schema: str, table_name: str):
    """create table using custom transformation from pandas dtypes to sql types. Used for turbodbc.

    Args:
        columnas (list): columns of the table
        df (pandas.Dataframe): pandas dataframe used as reference of types
        schema (str): schema name
        table_name (str): table name

    Returns:
        [string]: sql sentece for table creating
    """
    print("create_table")
    print("Tipos disponibles:")
    print(df.dtypes)
    create_columns = ",".join(
        [
            f"{col} " + convertir_tipo(col, str(df[col].dtype)) + " NULL"
            for col in columnas
        ]
    )
    sql = f"""CREATE TABLE [{schema}].[{table_name}] (
                {create_columns}
              )
            """
    return sql


def schema_exists(cursor, schema: str):
    """checks whether a schema exists in a database or not

    Args:
        cursor (sqlalchemy.engine): database conection
        schema (str): schema name

    Returns:
        [bool]: existence of the schema
    """
    cursor.execute(
        f"""SELECT  *
                    FROM    sys.schemas
                    WHERE   name = N'{schema}';
            """
    )
    a = cursor.fetchone()
    return a is not None


def truncate_table(schema_name: str, table_name: str):
    """sql sentence for truncate table operation

    Args:
        schema (str): schema name
        table_name (str): table name

    Returns:
        [string]: sql sentece for truncate table operation
    """
    sql = f"""TRUNCATE TABLE {schema_name}.{table_name};"""
    return sql


def drop_table(schema_name: str, table_name: str) -> None:
    """SQL code for drop table
    
    Args:
        schema_name (str): schema name
        table_name (str): table name

    Returns:
        [str]: SQL query
    """
    sql = f""" IF (EXISTS (SELECT 1 
                            FROM INFORMATION_SCHEMA.TABLES 
                            WHERE TABLE_SCHEMA = '{schema_name}' 
                            AND  TABLE_NAME = '{table_name}'))
                BEGIN
                    DROP TABLE {schema_name}.{table_name};
                END"""
    return sql 
