from enum import Enum, auto

class ConnType(Enum):
    """
    ConnType [Enum for connection types]

    Args:
        Enum ([type]): Connection types
    """
    google_cloud_platform = auto()
    mysql = auto()
    mssql = auto()
