from abc import ABC, abstractmethod

from sqlalchemy.engine.base import Engine

class SqlOperations(ABC):

    def __init__(self) -> None:
        print("Sql operations init")

    @abstractmethod
    def get_connection_string(self, conn_object: dict) -> str:
        pass

    @abstractmethod
    def get_engine(self, conn_object: dict) -> Engine:
        pass
    
    @abstractmethod
    def truncante_table(self, schema_name: str, table_name: str) -> str:
        pass
    
    @abstractmethod
    def create_table(self, schema: str, table_name: str) -> str:
        pass
    
    @abstractmethod
    def exist_schema(self, schema: str) -> str:
        pass
    
    @abstractmethod
    def exist_table(self, schema: str, table_name: str) -> str:
        pass
    
    @abstractmethod
    def drop_table(self, schema_name: str, table_name: str):
        pass
    
    @abstractmethod
    def merge(
        self,
        columns: list,
        merge_ids: list,
        target_db: str,
        target_schema: str,
        source_db: str,
        source_schema: str,
        table_name: str,
        merge_method: str,
    ) -> str:
        pass
    
    @abstractmethod
    def get_column_names(self, schema: str, table_name: str) -> str:
        pass
    
    @abstractmethod
    def delete_from_table(self, schema: str, table_name: str, filter: str) -> str:
        pass
    

    