from include.utilitiesModule.SqlOperations import SqlOperations
from sqlalchemy.engine.base import Engine
from sqlalchemy.engine import create_engine


class MsSqlOperations(SqlOperations):
    """
    MsSqlOperations [Sql queries adaptation for Ms Sql server (T-SQL)]

    Args:
        SqlOperations (include.utilitiesModule.SqlOperations): Abstract class for sql queries
    """
    def __init__(self) -> None:
        """
        __init__ [Initialization method]
        """
        print("Ms Sql operation init")
        super().__init__()

    def get_connection_string(self, conn_object: dict) -> str:
        """
        get_connection_string [Connection string for sql alchemy]

        Args:
            conn_object (dict): Conn object, with necessary information for connection 
                string building. Need it values: host, schema, login, password, extra, port

        Returns:
            str: Sql alchemy connection string
        """
        print("MsSqlOperations.get_connection_string")
        host = conn_object.host
        db = conn_object.schema
        login = conn_object.login
        password = conn_object.password
        extra = conn_object.extra
        if extra is None:
            extra = ""
        driver = "{ODBC Driver 17 for SQL Server}"

        print("variables------------------")
        for item in [host, db, login, password, extra]:
            print(item)

        port = ":" + str(conn_object.port) if conn_object.port is not None else ""
        print("Using SQL Alchemy")
        connection_string = (
            "mssql+pyodbc://"
            + login
            + ":"
            + password
            + "@"
            + host
            + port
            + "/"
            + db
            + "?driver=ODBC+Driver+17+for+SQL+Server"
        )

        return connection_string

    def get_engine(self, conn_object: dict) -> Engine:
        """Return sql alchemy engine

        Args:
            conn_object (dict): dictionary for engine creation

        Returns:
            [Engine]: sql alchemy engine
        """
        connection = self.get_connection_string(conn_object=conn_object)
        engine = create_engine(connection)
        return engine

    def truncante_table(self, schema_name: str, table_name: str) -> str:
        """sql sentence for truncate table operation

        Args:
            schema (str): schema name
            table_name (str): table name

        Returns:
            [string]: sql sentece for truncate table operation
        """
        sql = f"""TRUNCATE TABLE {schema_name}.{table_name};"""
        return sql

    def create_table(self, schema: str, table_name: str) -> str:
        raise NotImplementedError()

    def exist_schema(self, schema: str) -> str:
        """checks whether a schema exists in a database or not

        Args:
            schema (str): schema name

        Returns:
            [str]: query for check schema existence
        """
        tsql = f"""SELECT  *
                    FROM    sys.schemas
                    WHERE   name = N'{schema}';
            """
        return tsql

    def exist_table(self, schema: str, table_name: str) -> str:
        """checks whether a table exists in a database or not

        Args:
            schema (str): table schema name
            table_name (str): table name

        Returns:
            [str]: query for check the existence of the table
        """
        tsql = f"""IF EXISTS
            (
            SELECT 
                    *
                FROM INFORMATION_SCHEMA.TABLES WITH (NOLOCK)
                WHERE TABLE_NAME = '{table_name}' 
                AND TABLE_SCHEMA = '{schema}'
            )
            BEGIN
                SELECT 
                    'Your table exists.' AS result;
            END;
        """
        return tsql

    def drop_table(self, schema_name: str, table_name: str) -> str:
        """SQL code for drop table

        Args:
            schema_name (str): schema name
            table_name (str): table name

        Returns:
            [str]: SQL query
        """
        sql = f"""IF (EXISTS (SELECT 1 
		     FROM INFORMATION_SCHEMA.TABLES 
		     WHERE TABLE_SCHEMA = '{schema_name}' 
		      AND  TABLE_NAME = '{table_name}'))
                    BEGIN
                        DROP TABLE {schema_name}.{table_name};
                    END;"""
        return sql

    def get_column_names(self, schema: str, table_name: str) -> str:
        """SQL code to get column names of a table

        Args:
            schema_name (str): schema name
            table_name (str): table name

        Returns:
            [str]: SQL query
        """

        sql = f"""SELECT 
                   COLUMN_NAME
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_SCHEMA = '{schema}'
                    AND TABLE_NAME = '{table_name}'
                ORDER BY 
                    ORDINAL_POSITION;
           """
        return sql

    def merge_condition(self, merge_ids: list) -> str:
        """String with sql merge condition based on a id list

        Args:
            merge_ids (list): List of column names that are included on merge condition

        Returns:
            [str]: String with merge condition
        """
        lista = [f"a.{columna}=b.{columna}" for columna in merge_ids]
        return " and ".join(lista) if len(lista) > 1 else lista[0]

    def merge(
        self,
        columns: list,
        merge_ids: list,
        target_db: str,
        target_schema: str,
        source_db: str,
        source_schema: str,
        table_name: str,
        merge_method: str,
    ) -> str:
        """Merge query

        Args:
            columns (list): List of column names that are included on merge condition
            merge_ids (list): List of column names that are included on merge condition
            target_db (str): Name of the target database
            target_schema (str): Name of the target schema
            source_db (str): Name of the source database
            source_schema (str): Name of the source schema
            table_name (str): Table name
            merge_method (str): Whether the method will delete not matching values or not 
                ('WHEN NOT MATCHED BY SOURCE' condition of the query). Possible values: 
                'append' or 'replace'

        Returns:
            [str]: String with merge query
        """

        print("columnas")
        print(columns)
        print(len(columns))

        matching_columns = ",".join([f"a.{col} = b.{col}" for col in columns])
        insert_columns = ",".join([col for col in columns])

        merge_keys = self.merge_condition(merge_ids)

        merge_query = f"""
        MERGE {target_db}.{target_schema}.{table_name} a
        USING {source_db}.{source_schema}.{table_name} b
        ON ({merge_keys})
        WHEN MATCHED 
            THEN UPDATE SET {matching_columns}
        WHEN NOT MATCHED BY TARGET
            THEN
            INSERT ({insert_columns})
            VALUES ({insert_columns})
        """
        if merge_method == "replace":
            merge_query += """
            WHEN NOT MATCHED BY SOURCE
                THEN DELETE
            """

        merge_query += ";"

        print(f"Merge method: {merge_method}")
        print(f"Query para el merge: \n {merge_query}")

        return merge_query

    def delete_from_table(self, schema: str, table_name: str, filter: str) -> str:
        """String with sql merge condition based on a id list

        Args:
            schema (str): schema name
            table_name (str): table name
            filter (str): filter to include on where (Example: 'date_field > GETDATE()' )

        Returns:
            [str]: String for delete query with filter (if exists)
        """
        if len(filter) > 0:
            filter = " AND " + filter + " "
        else:
            filter = ""

        sql_template = f"""DELETE FROM {schema}.{table_name} (NOLOCK)
                            WHERE 1 = 1
                            {filter}
                        """
        return sql_template
